// From a children’s song

public class GreenBottles {
  public static void main(String[] args) {
    int numBottles = 10;

    while (numBottles > 0) {
      if (numBottles > 2) {
        System.out.println(numBottles + " green bottles of beer on the wall,");
        System.out.println(numBottles + " green bottles of beer on the wall!");
        System.out.println("And if one green bottle should accidentally fall,");
        numBottles = numBottles - 1;
        System.out.println("There’ll be " + numBottles + " green bottles of beer on the wall!\n");
      }
      if (numBottles == 2) {
        System.out.println(numBottles + " green bottles of beer on the wall,");
        System.out.println(numBottles + " green bottles of beer on the wall!");
        System.out.println("And if one green bottle should accidentally fall,");
        numBottles = numBottles - 1;
        System.out.println("There’ll be 1 green bottle of beer on the wall!\n");
      }
      if (numBottles == 1) {
        System.out.println(numBottles + " green bottle of beer on the wall,");
        System.out.println(numBottles + " green bottle of beer on the wall!");
        System.out.println("And if that green bottle should accidentally fall,");
        System.out.println("There’ll be no green bottles of beer on the wall!");
        numBottles = numBottles - 1;
      }
    }
  }
}
