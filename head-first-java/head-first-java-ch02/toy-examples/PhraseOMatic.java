public class PhraseOMatic {
  public static void main(String[] args) {
    // None of the words here begins with a vowel so that the indefinite
    // article ‘a’ can be used without issue.
    String[] wordListOne = {
        "platform-agnostic",
        "highly-opinionated",
        "voice-activated",
        "haptically-driven",
        "maintainable",
        "reactive",
        "functional",
        "strongly-typed",
        "reliable"
    };

    String[] wordListTwo = {
        "loosely-coupled",
        "six sigma",
        "asynchronous",
        "event-driven",
        "pub-sub",
        "IoT",
        "cloud native",
        "service-orientated",
        "containerised",
        "serverless",
        "microservices",
        "distributed ledger",
        "AI-enabled",
        "extensible"
    };

    String[] wordListThree = {
        "framework",
        "library",
        "DSL",
        "REST API",
        "repository",
        "pipeline",
        "service mesh",
        "architecture",
        "perspective",
        "design",
        "orientation"
    };

    int oneLength = wordListOne.length;
    int twoLength = wordListTwo.length;
    int threeLength = wordListThree.length;

    java.util.Random randomGenerator = new java.util.Random();
    int rand1 = randomGenerator.nextInt(oneLength);
    int rand2 = randomGenerator.nextInt(twoLength);
    int rand3 = randomGenerator.nextInt(threeLength);

    String phrase = wordListOne[rand1] + ", " + wordListTwo[rand2] + " " + wordListThree[rand3];

    System.out.println("What we need is a " + phrase + ".");
  }
}
