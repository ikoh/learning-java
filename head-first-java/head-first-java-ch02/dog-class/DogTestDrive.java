// Tester class to create objects of class Dog
public class DogTestDrive {
    public static void main(String[] args) {
        Dog d = new Dog();
        d.weightKg = 20;
        d.breed = "Whippet";
        d.bark();

        Dog2 d2 = new Dog2(20, "Whippet", "Rover");
        d2.bark();
    }
}

class Dog {
    int weightKg;
    String breed;
    String name;

    void bark() {
        System.out.println("Woof woof!");
    }
}

class Dog2 {
    int weightKg;
    String breed;
    String name;

    Dog2(int w, String b, String n) {
        this.weightKg = w;
        this.breed = b;
        this.name = n;
    }

    void bark() {
        System.out.println("My " + this.breed + " goes: ‘woof woof’!");
    }
}