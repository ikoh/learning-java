class DogTestDrive {
    public static void main(String[] args) {
        Dog[] petDogs = new Dog[2];
        petDogs[0] = new Dog(15, "Whippet", "Whippy");
        petDogs[1] = new Dog(25, "German Shepherd", "Spike");
        petDogs[1].bark();
    }
}

class Dog {
    int weightKg;
    String breed;
    String name;

    Dog(int weightKg, String breed, String name) {
        this.weightKg = weightKg;
        this.breed = breed;
        this.name = name;
    }

    void bark() {
        System.out.println("My " + this.breed + " goes: ‘woof woof’!");
    }
}
