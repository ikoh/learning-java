public class SongTestDrive {
    public static void main(String[] args) {
        Song song1 = new Song("Dance of Eternity", "Dream Theater");
        Song song2 = new Song("Crush", "Polyphia");
        song1.play();
        song2.play();
    }
}

class Song {
    String title;
    String artist;

    Song(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    void play() {
        System.out.println("Playing ‘" + this.title + "’ by " + this.artist);
    }
}