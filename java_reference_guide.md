# Java Reference Guide

A brain dump of noteworthy things I’ve learnt through resources or personal experience, organised alphabetically.

[TOC]

## Classes and objects

A class is a template for creating objects, like a factory machine that assembles parts together into a working product. A class holds information about what an object _knows_ (its **instance variables**) and what an object _does_ (its **methods**).

An `Employee` class may have the following instance variables:

- `manager`
- `name`
- `role`

It may have the following methods:

- `assignNewManager`
- `changeRole`
- `deactivateEmployeeStatus`

A specific employee’s information would be an instantiation of this class and an object with its own specific values.

## Classes: a basic example

This example is based on the code below. It is a hybrid of the `Dog` example in _Head First Java, 3e_ and [this CS Dojo YouTube video](https://www.youtube.com/watch?v=8yjkWGRlUmY) introducing classes and objects.

```java
public class DogTestDrive {
    public static void main(String[] args) {
        Dog d = new Dog();
        d.weightKg = 20;
        d.breed = "Whippet";
        d.bark();

        Dog2 d2 = new Dog2(20, "Whippet", "Rover");
        d2.bark();
    }
}

// Does not use a constructor
class Dog {
    int weightKg;
    String breed;
    String name;

    void bark() {
        System.out.println("Woof woof!");
    }
}

// Uses a constructor
class Dog2 {
    int weightKg;
    String breed;
    String name;

    Dog2(int w, String b, String n) {
        this.weightKg = w;
        this.breed = b;
        this.name = n;
    }

    void bark() {
        System.out.println("My " + this.breed + " goes: ‘woof woof’!");
    }
}

```

We start with the basic `Dog` class, which comprises two components:

- Instance variables (or attributes): `weightKg`, `breed` and `name`
- Method: `bark`

These pieces of information help a `Dog` object, which is an instantiation of the `Dog` class, know what properties it has and what it can do. The `void` before `bark` refers to the **return value** of the method, which in this case is nothing because printing is a side-effecting operation.

With `Dog`, every time we want to create an instance of this class, we have to define its instance variables line-by-line. This is somewhat cumbersome, and a more convenient way of instantiating the class is to do it the `Dog2` way.

The `Dog2` class has two changes from `Dog`:

- It includes a **class constructor** called `Dog2` (same name as the class, but without a return value appearing before its name)
- I’ve personalised the `bark` method somewhat

The `Dog2` constructor allows for the creation of a `Dog2` object by inserting appropriate values as parameters. This does mean that the following lines **wouldn’t** work any more:

```java
Dog2 d2 = new Dog2();
d2.weightKg = 20;
d2.breed = "Whippet";
d2.name = "Rover";
```

In the `this.*` convention, `this` refers to the object (i.e. class instance). Hence, in the case of `Dog2` and the example in `main`, `this.name` is the same as `d2.name`.

In conclusion, this example showed two ways of creating a basic Java class. One uses a constructor and the other does not.

## Installing Java

### Install SDKMAN!

Follow the instructions [here](https://sdkman.io/install). I learnt about SDKMAN! from this [Stack Overflow thread](https://stackoverflow.com/questions/69875335/macos-how-to-install-java-17), where a couple of commenters rated the tool highly. It’s like [`ghcup`](https://www.haskell.org/ghcup/) on steroids in that it allows the parallel installation of multiple SDKs (such as JVM-based programming languages).

### Install Java

Usage instructions for SDKMAN! are [here](https://sdkman.io/usage); the full list of install commands can be found by typing `sdk list` in the command line. In the case of Java it would be:

```
sdk install java
```

This is what I saw after running the command on 2023-05-12:

```
Downloading: java 17.0.7-tem

In progress...

####################################################################################################################################################### 100.0%

Repackaging Java 17.0.7-tem...

Done repackaging...
Cleaning up residual files...

Installing: java 17.0.7-tem
Done installing!


Setting java 17.0.7-tem as default.
```

Specific versions can also be installed by specifying the version number after the install command (e.g. `sdk install java x.x.x`).

Note that the vendor of Java I got in SDKMAN! is **Temurin**. I saw this by running `sdk list java` in the command line. (The `-tem` appended to the end of the Java version indicates the vendor.)

Here are some Java versions with different vendors:

- `java 17.0.7-tem` (Temurin)
- `java 17.0.7-ms` (Microsoft)
- `java 17.0.7-oracle` (Oracle)
- `java 17.0.7-amzn` (Corretto)

### Uninstall Java

This removes the local version that is installed, but not the local installation.

```
sdk uninstall java x.x.x
```

## `main`

The `main` function/method has two purposes, as far as I can tell from _Head First Java_ (chapter 2):

1. To test a class
2. To launch or start a Java application

Here’s a snippet of the chapter:

> A real Java application is nothing but objects talking to other objects. In this case, _talking_ means objects calling methods on one another.

## Memory management: an introduction

At the very least, learn about terms such as _garbage collection_ and the _heap_ (where objects are stored; also known as the _garbage-collectible heap_). See this [Oracle document](https://docs.oracle.com/cd/E13150_01/jrockit_jvm/jrockit/geninfo/diagnos/garbage_collect.html) for more information about the heap, nursery and garbage collection.

## Memory management: simple example involving the heap

This comes from chapter 3 of _Head First Java (3e)_ – please refer to the chapter because it contains diagrams that help with understanding. For this entry, though, I’ll walk through the example and explain what’s happening through text.

```java
// Assume the Book class is predefined
Book b = new Book();
Book c = new Book();
```

- `b` and `c` are object reference variables, and are instantiations of the `Book` class
- Two separate objects are created on the garbage-collectible heap
- For simplicity, let’s associate `b` with object 1, and `c` with object 2; therefore, `b` has a reference to object 1 and `c` has a reference to object 2

```java
b = c;
```

- The direction of assignment goes from left to right (I think); everything in `c` is copied and then handed over to `b`
- This means that, now, `b` and `c` have references to object 2
- Object 1 no longer has any variable referencing it, which makes it a candidate for garbage collection
- We can say that object 1 is an abandoned object, while object 2 remains a reachable object

```java
c = null;
```

- `null` is a value
- By assigning `null` to `c`, the reference from `c` to object 2 is gone (object 2 is not a candidate for GC because it still has `b` referencing it)
- `c` is now a null reference

## Objects: state influences behaviour

See the following example.

```java
public class SongTestDrive {
    public static void main(String[] args) {
        Song song1 = new Song("Dance of Eternity", "Dream Theater");
        Song song2 = new Song("Crush", "Polyphia");
        song1.play();
        song2.play();
    }
}

class Song {
    String title;
    String artist;

    Song(String title, String artist) {
        this.title = title;
        this.artist = artist;
    }

    void play() {
        System.out.println("Playing ‘" + this.title + "’ by " + this.artist);
    }
}
```

Here, the `play` method prints out a statement, but we can also pretend that it actually starts the playback of a song.

Because `play` _depends_ on the `title` and `artist` instance variables, and instance variables represent an object’s state, we say that the object’s state _influences_ the behaviour of its methods. Note that `play` takes no parameters but simply uses the values of `title` and `artist` as defined in the `Song` constructor.

## Run Java without compiling

```
java MyFile.java
```

Using `javac` instead of `java` compiles the file and produces bytecode. Hence, `java x.java` is like using `runghc` on an `x.hs` file.

## Types: primitives

Below are the eight primitive types in Java:

- `boolean`
- `char`
- `byte`
- `short`
- `int`
- `long`
- `float`
- `double`

Notice that `String` isn’t one of them! Also note that these primitives start with a **lowercase** letter.

| **Type**               | **Bit depth** | **Value range**               |
| ---                    | ---           | ---                           |
| `boolean`              | JVM specific  | `true` or `false`             |
| `char 16`              | bits          | `0` to `65535`                |
| `byte` (numeric type)  | 8 bits        | `-128` to `127`               |
| `short` (numeric type) | 16 bits       | `32768` to `32767`            |
| `int`                  | 32 bits       | `-2147483648` to `2147483647` |
| `long`                 | 64 bits       | –huge to huge                 |
| `float`                | 32 bits       | variable                      |
| `double`               | 64 bits       | variable                      |

## Variables: declaring a variable

Variable declaration requires two pieces of information on the user’s part:

- Type (could be built-in/primitive or an object reference; helps the JVM predict the variable’s maximum size)
- Name

We can think of a **variable as a container**. Such a container would have a **size**, something that it can **hold**, as well as a **label** so that we know what the container is used for.

A Java variable holds a type or object reference. The type or object reference determines the variable’s size. During declaration, the user specifies a name for the variable so that it can be used in different parts of the code. Below are some examples of variable declarations (without assignment).

```java
int weightKg;
boolean isJavaWorthMyTime;
double annualSalaryEuro;
```

Note that the JVM errs on the side of caution, so it won’t allow us to assign larger types to smaller ones during variable declarations. That is, if we have an `int` variable, we wouldn’t be allowed to assign it to another variable of `boolean` type, since the latter only has a maximum of two distinct values and so is much smaller in size.