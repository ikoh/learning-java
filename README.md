# Learning Java

## Resources

- [_Head First Java_ (3e)](https://learning.oreilly.com/library/view/head-first-java/9781492091646/)
- [_Java in a Nutshell_ (8e)](https://learning.oreilly.com/library/view/java-in-a/9781098130992/)